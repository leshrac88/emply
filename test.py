# -*- coding: utf-8 -*-

import unittest
import time
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

HOST_NAME = 'https://emply.ru/'
WAGE = 70000
CITY = "Казань"

class TestAssertSeleniumTitle(unittest.TestCase):
    
  def setUp(self):
    self.browser = webdriver.Firefox()
    self.browser.implicitly_wait(10)
    self.browser.get(HOST_NAME)

  def test_search_by_user(self):
    browser = self.browser
    
    # select CITY
    browser.find_element_by_id('areas_ref').click()
    browser.find_element_by_id('search_areas-10007').click()
    browser.find_element_by_id('search_areas-32').click()
    browser.find_element_by_id('search_areas-118').click()
    self.click_ok()
    self.wait_for_ajax()

    # select department
    browser.find_element_by_xpath('//div[@data-btnname="industry"]/div/a').click()
    browser.find_element_by_id('search_industry-374').click()
    time.sleep(2)
    browser.find_element_by_xpath('//label[@for="search_filter_industry_selectall_cb"]/span').click()
    self.click_ok()
    self.wait_for_ajax()

    # select wage
    browser.find_element_by_xpath('//div[@data-btnname="wage"]/div/a').click()
    browser.find_element_by_id('amount').send_keys(str(WAGE))
    self.click_ok()
    self.wait_for_ajax()

    # go to first vacancy
    href = browser.find_element_by_xpath('//ul[@class="search_ctrl_main_result_block_list_wrapper_list row"]/li[1]/div/div/h3/a').get_attribute("href")
    browser.find_element_by_xpath("//body").send_keys(Keys.CONTROL + "t")
    browser.get((HOST_NAME + href) if href.find("https://") < 0 else href)
    self.wait_for_ajax()

    # get info from page
    city_from_page = browser.find_element_by_xpath('//span[@class="vac_item_header_right_header_city"]').text
    wage_from_page = browser.find_element_by_xpath('//div[@class="vac_item_body_header_right_money"]').text
    wage_count = re.findall('(\d+)', wage_from_page)

    # convert wage[] to int for right min-max result
    wage_convert = []
    for element in wage_count:
      wage_convert.append(int(element))

    if len(wage_convert) == 1:
      self.assertGreaterEqual(wage_convert[0], WAGE) # if one wage in vacancy
    elif len(wage_convert) == 2:
      self.assertGreaterEqual(WAGE, min(wage_convert)) and assertLessEqual(WAGE, max(wage_convert)) #if range in vacancy
    else:
      print('[WARN ]: Found {0} match in wage'.format(len(wage_convert)))

    self.assertEqual(CITY, city_from_page)

  def tearDown(self):
    self.browser.quit()

  def click_ok(self):
    self.browser.find_element_by_xpath('//footer/button[@class="search_filter_slider_okbtn btn btn-primary ok"]').click()

  def wait_for_ajax(self):
    time.sleep(0.5)
    counter = 0
    while int(self.browser.execute_script("return $.active")) > 0:
      counter += 1
      time.sleep(0.1)
      if counter >= 200:
        raise Exception("AJAX request took longer than 5 seconds.")
      
    time.sleep(0.5)


if __name__ == '__main__':
  unittest.main(verbosity=2)